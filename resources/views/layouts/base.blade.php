<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="<?= csrf_token() ?>">

    <title>@yield('title', 'Laravel')</title>
    {!! HTML::styleLink('/assets/css/bower.css') !!}
    {!! HTML::styleLink('/assets/css/app.css') !!}

    @yield('additional.css')
</head>

<body>


<!-- Begin page content -->
<div class="container">
    @yield('content.base')
</div>


@include('layouts.partials.footer')


{!! HTML::scriptLink('/assets/js/bower.js') !!}
{!! HTML::scriptLink('/assets/js/app.js') !!}

<?php if (config('app.debug')){?>
{!! HTML::scriptLink('//localhost:35729/livereload.js') !!}
<?php } ?>

@yield('additional.js')
</body>
</html>
