# Laravel PHP Boilerplate

## Requirements
* Vagrant: http://vagrantup.com
* VirtuaBox: https://www.virtualbox.org

## Basic work with environment
1. Adjust Homestead.yaml file if need (set sites/map folder to your local **{{PROJECT}}/development** directory full path)
2. Create and start Homestead virtual machine with laravel application 
```
cd {{PROJECT}}\development
vagrant up
```
3. Access web-application on http://192.168.111.10:80 address (or whatever you define in Homestead.yaml). You can place this IP in you **hosts** file for easier access.  
4. Stop environment VM - to release runtime resources (CPU, RAM) temporary (if you want to continue work from same point later) 
```
vagrant halt
```
5. Continue work after you used *vagrant halt*
```
vagrant up
```
6. Destroy environment (when you don't need project VM anymore or to reset it from scratch)
```
vagrant destroy
```