# Synopsis
This is backend on **PHP / Laravel 5.2** for [???](https://crm.saritasa.com/projects/general.aspx?ProjectID=???) project

# Description
TODO: write description 

# UI Invision
TODO: setup link 

# Goolge Drive
TODO: setup link

# Issue Tracker
TODO: setup link
https://saritasa.atlassian.net/browse/???

# Setup
## Run and develop in isolated environment 
1. Install [VirtualBox](http://virtualbox.org) and [Vagrant](http://vagrantup.com)
2. 
```
[user]$ cd development
vagrant up
```
This will create new VM, download and install all required dependencies inside that VM.
3. Wait for download and configuration process to complete (first run may take up to 30 min). 
4. Open [http://192.168.111.10](http://192.168.111.10)
 (or whatever IP you set in development/Homestead.yaml file)
5. *development* folder will be shared with this environment - all changes to files in this folder

## Develop with full stack locally
Requirements:
* [PHP](https://php.net) 7.0+
* [MySQL](https://www.mysql.com) 5.7+
* [Composer](https://getcomposer.org) - PHP package manager
* [Node.JS](https://nodejs.org) - JavaScript runtime
* [NPM](https://npmjs.com) - Node.JS package manager
* [Bower](https://bower.io) - Package manager for web components
* [Grunt](https://gruntjs.com) - Build system

Install all required software from above list, then run:

```
[user]$ cd development
composer install
npm install
bower install
grunt build
```

Create Laravel's configuration file:

```
cp .env.example .env
```

Create empty MySQL database and set proper DB_* variable in **.env** file,
then create DB structure for current application version
(or update existing DB structure to current version):

```
php artisan migrate
```

Fill in database with some random values for testing:

```
php artisan db:seed
```