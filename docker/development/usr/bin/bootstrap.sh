#!/bin/bash

cd /home/www/app/

## Create folders for laravel
echo "Creating laravel environment"
mkdir -p /home/www/app/storage/{cache,debugbar,logs,meta,sessions,views}
chmod -R 777 /home/www/app/storage/logs/

### Generate robots.txt
if [[ "$APP_ENV" == "development" ]]; then
    echo "create robots.txt for development environment"
    echo -e "User-agent: *\nDisallow: /" > /home/www/app/public/robots.txt
fi

### Generate laravel .env file
mv .env.$APP_ENV .env

### Install dependencies
composer install --prefer-source --no-interaction

### Run migrations
php artisan migrate

### Chown app dir
echo "Owning app dir by nginx"
chown -R nginx:nginx /home/www/app

### Start supervisord
echo "Starting nginx"
supervisorctl start nginx

echo "Starting php-fpm"
supervisorctl start php

echo "Clean up and exit"
exit 0
