server {
    listen 80 default;
    server_name webapp.la;
    root "/home/www/app/public";
    index index.html index.htm index.php;
    charset utf-8;

    client_header_timeout 300;
    client_max_body_size 32m;
    client_body_timeout 300;
    send_timeout 300;
    proxy_connect_timeout 300;
    proxy_read_timeout 300;
    proxy_send_timeout 300;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
    access_log off;
    error_log  /var/log/nginx/webapp-error.log error;

    sendfile off;
    tcp_nopush on;
    tcp_nodelay on;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/webapp_fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_read_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_connect_timeout 300;
        fastcgi_param  HTTPS "on";
    }

    # Any files matching these extensions are cached for a long time
    location ~* \.(jpg|jpeg|png|gif|ico|css|js|ttf|woff|woff2|svg)$ {
       expires max;
       add_header Cache-Control public;
       access_log off;
    }

    location ~ /\.ht {
        deny all;
    }
}
