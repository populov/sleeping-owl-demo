<?php

namespace App;

use App\Extensions\Traits\ValidationTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;


/**
 * App\User
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Bican\Roles\Models\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Bican\Roles\Models\Permission[] $userPermissions
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDeletedAt($value)
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, HasRoleAndPermissionContract
{

    use Authenticatable, CanResetPassword, ValidationTrait, HasRoleAndPermission;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id',
        'name',
        'email'
    ];

    /**
     * The attributes that should be visible in arrays for self profile.
     *
     * @var array
     */
    protected $profileVisible = [
        'id',
        'name',
        'email',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
        'remember_token',
        'created_at',
        'updated_at',
        'api_token'
    ];

    /**
     * Set visible attributes for self profile
     *
     * @return User
     */
    public function setProfileVisible()
    {
        $this->setVisible($this->profileVisible);
        return $this;
    }

    /**
     * Return array of rules for model validation
     *
     * @return array
     */
    public function getRules()
    {
        $rules = [
            'name'  => "required|max:255",
            'email'     => "required|max:255|email|unique:users,email,{$this->id},id"
        ];
        if (!$this->id) {
            $rules['password'] = 'required|min:5|max:50';
        }
        return $rules;
    }

    /**
     * Validate password
     *
     * @param string $password
     *
     * @return boolean
     */
    public function passwordCheck($password)
    {
        return password_verify($password, $this->password);
    }

    /**
     * Encodes the user's password.
     *
     * @param string $password
     * @return string
     * @access public
     * @static
     */
    public static function encodePassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
    
    /**
     * Sets the user's password.
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = self::encodePassword($password);
    }
}