<?php

namespace App\Model\Repositories;

use App\Extensions\Traits\SearchTrait;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\MessageBag;


abstract class BaseRepository
{
    use SearchTrait;

    /**
     * @var MessageBag
     */
    protected $errors;


    /**
     * @var array
     */
    protected $searchebleFields = [];


    /**
     * Execute a Closure within a transaction.
     *
     * @param \Closure $callback
     * @return mixed
     * @throws \Throwable
     */
    public function transaction(\Closure $callback)
    {
        return DB::transaction($callback);
    }


    /**
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }

    /**
     * Added error to message bag. If bag not exists then it will be created.
     *
     * @param $key
     * @param $error
     */
    protected function addError($key, $error)
    {
        if (!$this->errors) {
            $this->errors = new MessageBag();
        }
        $this->errors->add($key, $error);
    }


    /**
     * Get visible fields from model
     *
     */
    public function getVisibleFields()
    {
        $model = $this->getModel();
        return $model->getVisible();
    }


    /**
     * Get new model of model by id (used for store method)
     *
     * @param $id
     * @return Model
     */
    public function getModelByID($id = null)
    {
        $model = $this->getModel();
        if (!empty($id)) {
            $model = $model->find($id);
        }
        if (!$model) {
            $this->errors = new MessageBag(['Model not found.']);
            return null;
        }
        return $model;
    }

    /**
     * Get model of repository
     *
     */
    abstract function getModel();

    /**
     * Get list of searcheble fields
     *
     */
    abstract function getSearchFields();
}