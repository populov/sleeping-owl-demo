<?php

namespace App\Model\Repositories;

use App\Api\V1\Exceptions\ModelException;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserRepository extends BaseRepository
{
    public function getModel()
    {
        return new User();
    }

    public function getSearchFields()
    {
        return [
            'name',
            'email'
        ];
    }

    /**
     * Validate input and login user on success.
     *
     * @param array $credentials
     *
     * @return Token if login succeed else null.
     */
    public function JWTAuth(array $credentials)
    {
        try {
            if (!$token = \JWTAuth::attempt($credentials)) {
                throw new ModelException(401, 'Invalid email or password');
            }
        } catch (JWTException $e) {
            throw new ModelException(500, 'Could not create token');
        }
        return $token;
    }

    /**
     * Store user to database
     *
     * @param  integer $id
     * @param  array $input
     * @return mixed
     */
    public function store($id, array $input)
    {
        if (!$user = $this->getModelByID($id)) {
            return false;
        }
        $user->fill($input);
        if (!empty($input['password'])) {
            $user->password = $input['password'];
        }

        if (!$user->isValid(array_merge($user->toArray(), $input))) {
            $this->errors = $user->errors();
            return false;
        }
        $user->save();
        return $user;
    }


    /**
     * Delete User
     *
     * @param  integer $id
     * @return mixed
     */
    public function destroy($id)
    {
        if (!$user = $this->getModelByID($id)) {
            return false;
        }
        $user->delete();
        return true;
    }

    /**
     * Get user details
     *
     * @param integer $userId
     * @param User $authenticatedUser
     * @return User
     */
    public function getProfile($userId, User $authenticatedUser)
    {
        $user = User::find($userId);
        if ($user->id == $authenticatedUser->id) {
            $user->setProfileVisible();
        }
        return $user;
    }
}