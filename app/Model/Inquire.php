<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Request from company to place there logo on a billboard
 *
 * Class Inquire
 *
 * @package App\Model
 * @property integer $id
 * @property string $email
 * @property string $name
 * @property string $company_name
 * @property string $company_address
 * @property string $phone
 * @property integer $template_id
 * @property string $custom_logo
 * @property integer $market_place_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Placement[] $placements
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Model\Placement[] $activePlacements
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereCompanyName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereCompanyAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereTemplateId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereCustomLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereMarketPlaceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Inquire isNew()
 */
class Inquire extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function placements() {
        return $this->hasMany(Placement::class);
    }

    public function activePlacements() {
        return $this->hasMany(Placement::class)
            ->where('payed_since', '>', Carbon::now())
            ->where('payed_until', '<', Carbon::now());
    }

    /**
     * All records added today, considered as new
     * @param Builder $query
     * @return $this
     */
    public function scopeIsNew(Builder $query) {
        return $query->where('created_at', '>', Carbon::yesterday()->toDateString());
    }
}
