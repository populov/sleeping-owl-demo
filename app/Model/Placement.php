<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * A record, where an inquire is placed
 *
 * Class Placement
 *
 * @package App\Model
 * @property integer $id
 * @property integer $market_place_id
 * @property integer $inquire_id
 * @property string $payed_since
 * @property string $payed_until
 * @property string $deleted_at
 * @property-read \App\Model\MarketPlace $marketPlace
 * @property-read \App\Model\Inquire $inquire
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Placement whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Placement whereMarketPlaceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Placement whereInquireId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Placement wherePayedSince($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Placement wherePayedUntil($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Model\Placement whereDeletedAt($value)
 * @mixin \Eloquent
 */
class Placement extends Model
{
    /**
     * @var bool indicates if the model should be timestamped
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'payed_since',
        'payed_until',
        'deleted_at',
    ];

    public function marketPlace() {
        return $this->belongsTo(MarketPlace::class);
    }

    public function inquire() {
        return $this->belongsTo(Inquire::class);
    }
}
