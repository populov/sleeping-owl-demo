<?php

namespace App\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use App\Model\MarketPlace as MarketPlaceModel;
use App\Model\Template as TemplateModel;

class MarketPlace extends Section implements Initializable
{
    protected $checkAccess = false;
    protected $title = "Market Places";
    protected $alias = "markets";
    protected $icon = "fa fa-bank";

    /**
     * Initialize class.
     */
    public function initialize()
    {
        // note: isFree() is a scope method in the model
        $freeCount = MarketPlaceModel::isFree()->count();
        $badge = $freeCount ? 'free: ' . $freeCount : null;

        $this->addToNavigation(200, $badge);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setColumns([
            AdminColumn::link('route'),
            AdminColumn::text('city'),
            AdminColumn::text('state'),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            // note: setDisplay() declares a column of DB table, which uses as visible options names
            AdminFormElement::select('template_id', 'Template')->required()
                ->setModelForOptions(new TemplateModel())->setDisplay('name'),
            AdminFormElement::text('state', 'State')->required(),
            AdminFormElement::text('city', 'City'),
            AdminFormElement::text('zip', 'ZIP code')->addValidationRule('regex:/\d{4,7}/'),
            AdminFormElement::text('route', 'Route')->required(),
            AdminFormElement::text('mile', 'Mile')->required()->addValidationRule('numeric'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
