<?php

namespace App\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use Carbon\Carbon;

class Placement extends Section implements Initializable
{
    const US_DATE_FORMAT = 'm/d/Y';
    /**
     * @var string title in menu list
     */
    protected $title = 'Sign Placements';

    /**
     * @var string URL part
     */
    protected $alias = 'placements';

    /**
     * @var string CSS classes for icon of menu item
     */
    protected $icon = 'fa fa-map-signs';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation($priority = 300);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setColumns([
            AdminColumn::custom('Market Place', function (\App\Model\Placement $placement) {
                $marketUrl = route('admin.model.edit', ['markets', $placement->market_place_id]);
                $marketName = $placement->marketPlace->city;
                return "<a href='$marketUrl'>$marketName</a>";
            }),
            AdminColumn::custom('Inquire', function (\App\Model\Placement $placement) {
                $inquireUrl = route('admin.model.edit', ['inquires', $placement->inquire_id]);
                $inquireName = $placement->inquire->company_name;
                return "<a href='$inquireUrl'>$inquireName</a>";
            }),
            AdminColumn::datetime('payed_since', 'Payed Since')->setFormat(self::US_DATE_FORMAT),
            AdminColumn::datetime('payed_until', 'Payed Until')->setFormat(self::US_DATE_FORMAT),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::select('market_place_id', 'Market Place')->required()
                ->setModelForOptions(new \App\Model\MarketPlace())->setDisplay('city'),
            AdminFormElement::select('inquire_id', 'Inquire')->required()
                ->setModelForOptions(new \App\Model\Inquire())->setDisplay('company_name'),
            AdminFormElement::date('payed_since', 'Payed Since')->setFormat(self::US_DATE_FORMAT)->required(),
            AdminFormElement::date('payed_until', 'Payed Until')->setFormat(self::US_DATE_FORMAT)->required(),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }
}
