<?php

namespace App\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use App\Model\Inquire as InquireModel;

class Inquire extends Section implements Initializable
{
    protected $checkAccess = true;
    protected $icon = 'fa fa-fax';
    protected $title = 'Inquiries';
    protected $alias = 'inquiries';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        // note: isNew() is a scope method in the model
        $newInquiresCount = InquireModel::isNew()->count();
        $badge = $newInquiresCount ? 'new: ' . $newInquiresCount : null;

        $this->addToNavigation(400, $badge);
    }

    /**
     * Page: list of inquiries
     * @return \SleepingOwl\Admin\Display\DisplayDatatables
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatables();
        $display->setColumns([
            AdminColumn::link('name')->setLabel('Contact Name'),
            AdminColumn::email('email')->setLabel('Email'),
            AdminColumn::text('company_name')->setLabel('Company Name'),
            AdminColumn::text('company_address')->setLabel('Company Address'),
            AdminColumn::text('phone')->setLabel('Phone'),
            AdminColumn::image('custom_logo')->setLabel('Custom Logo'),
            AdminColumn::datetime('created_at')->setLabel('Created')->setWidth('150px'),
        ]);
        return $display;
    }

    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Contact Name')->required()->addValidationRule('alpha'),
            AdminFormElement::text('email', 'E-mail')->required()->addValidationRule('email'),
            AdminFormElement::text('company_name', 'Company Name')->required(),
            AdminFormElement::text('company_address', 'Company Address')->required(),
            AdminFormElement::text('phone', 'Phone')->required()->addValidationRule('regex:/[+0-9][0-9\(\)\s-]{6,13}/', 'Enter a valid phone number'),
            AdminFormElement::image('custom_logo', 'Custom Logo'),
            AdminColumn::image('custom_logo'),
        ]);
    }

    public function onCreate()
    {
        return $this->onEdit(null);
    }
}