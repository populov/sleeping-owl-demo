<?php

namespace App\Admin;

use AdminColumn;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

/**
 * Here "Template" means a template of the banner, that can be placed on a road.
 */
class Template extends Section implements Initializable
{
    protected $checkAccess = false;
    protected $icon = "fa fa-newspaper-o";

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation();
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = \AdminDisplay::datatables();
        $display->setColumns([
            AdminColumn::link('name', 'Name')->setWidth('200px'),
            AdminColumn::image('background', 'Custom Logo')->setWidth('150px'),
            AdminColumn::datetime('created_at', 'Created'),
        ]);
        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Name')->required(),
            AdminFormElement::image('background', 'Background')->required(),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
