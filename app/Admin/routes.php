<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = 'Here will be the dashboard.';
	return AdminSection::view($content, 'Dashboard');
}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Some information here.';
	return AdminSection::view($content, 'Information');
}]);
