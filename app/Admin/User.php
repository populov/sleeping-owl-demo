<?php

namespace App\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;

class User extends Section implements Initializable
{
    protected $checkAccess = true;
    protected $icon = 'fa fa-users';

    public function initialize()
    {
        $this->addToNavigation(1000);
    }

    public function onDisplay() {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns([
                AdminColumn::link('name')->setLabel('Username'),
                AdminColumn::email('email')->setLabel('Email'),
                AdminColumn::datetime('created_at')->setLabel('Created')->setWidth('150px'),
                AdminColumn::datetime('updated_at')->setLabel('Last Login')->setWidth('150px')
            ])->paginate(20);
    }

    public function onEdit() {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Username')->required(),
            AdminFormElement::text('email', 'E-mail')->required()->addValidationRule('email'),
            AdminColumn::image('avatar')->setWidth('150px'),
        ]);
    }

    public function onCreate() {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Username')->required(),
            AdminFormElement::password('password', 'Password')->required()->addValidationRule('min:6'),
            AdminFormElement::text('email', 'E-mail')->required()->addValidationRule('email'),
        ]);
    }
}