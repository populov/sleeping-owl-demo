<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * @param User   $user
     * @param string $ability
     *
     * @return bool
     */
    public function before(User $user, $ability, User $item)
    {
        if ($ability != 'delete') {
            return true;
        }
    }

    public function delete(User $user, User $item) {
        return $item->id > 1            // Cannot delete default admin
            && $item->id != $user->id;  // User cannot delete himself
    }
}
