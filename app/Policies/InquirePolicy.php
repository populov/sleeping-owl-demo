<?php
namespace App\Policies;

use App\Model\Inquire;
use App\User;

class InquirePolicy {

    /**
     * Only admins can see that part of site
     * @param User $user Logged in user
     * @param string $ability Action to check for permissions
     * @param Inquire $inquire Object to check permissions
     * @return bool | null
     */
    public function before(User $user, $ability, Inquire $inquire)
    {
        return $user->is('admin');
    }

    /**
     * Only records without active placements can be deleted
     * @param User    $user
     * @param Inquire $inquire
     * @return bool
     */
    public function delete(User $user, Inquire $inquire)
    {
        return $inquire->activePlacements->count() > 0;
    }
}