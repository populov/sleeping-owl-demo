<?php

define('API_NS', '\App\Api\V1\Controllers\\');
define('WEB_NS', '\App\Http\Controllers\\');

$require = function ($routeFolder) {
    $directoryIterator = new DirectoryIterator($routeFolder);
    foreach ($directoryIterator as $directory) {
        if ($directory->isFile()) {
            require $routeFolder . $directory->getFilename();
        }
    }
};

$require(__DIR__ . '/Routes/web/');
$require(__DIR__ . '/Routes/API/v1/');

    
    

