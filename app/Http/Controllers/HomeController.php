<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;

/**
 * This class used for operation with payments
 */
class HomeController extends Controller
{

    /**
     * @route sessions.create
     * @return mixed
     */
    public function index()
    {
        return View::make('home.index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {

        return $this->renderContent(
            view('dashboard'),
            trans('http://localhost:8000/admin/users.dashboard')
        );
    }

    public function login()
    {
        return View::make('auth.login');
    }
}