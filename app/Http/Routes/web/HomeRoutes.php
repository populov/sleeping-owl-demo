<?php

Route::get('/', [
    'as'   => 'home.index',
    'uses' =>  WEB_NS . 'HomeController@index'
]);
Route::get('/home', function() {
    return redirect()->route('home.index');
} );

Route::get('/login', [
    'as'    => 'auth.login',
    'uses'  => WEB_NS . 'HomeController@login'
]);

Route::post('/login', [ 'uses' => WEB_NS.'Auth\AuthController@login']);

Route::get('/logout', ['as' => 'auth.logout', 'uses' => WEB_NS.'Auth\AuthController@logout']);