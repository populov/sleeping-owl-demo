<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->post('login', [
        'as' => 'auth.authenticate',
        'uses' => API_NS . 'AuthenticateController@authenticate'
    ]);

    $api->group(['middleware' => ['api.auth']], function ($api) {
        $api->delete('logout', [
            'as' => 'auth.logout',
            'uses' => API_NS . 'AuthenticateController@logout'
        ]);
    });

});



