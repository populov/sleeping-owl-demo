<?php
$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api){
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('users', [
            'as' => 'users.index',
            'uses' => API_NS . 'UserController@index'
        ]);

        $api->get('users/profile', [
            'as' => 'users.profile',
            'uses' => API_NS . 'UserController@profile'
        ]);
        
        $api->get('users/{id}', [
            'as' => 'users.show',
            'uses' => API_NS . 'UserController@show'
        ]);

        $api->delete('users/{id}', [
            'as' => 'users.destroy',
            'middleware' => ['middleware' => 'role:admin'],
            'uses' => API_NS . 'UserController@destroy'
        ]);

        $api->put('users/{id}', [
            'as' => 'users.update',
            'uses' => API_NS . 'UserController@update'
        ]);
    });

    $api->post('users', [
        'as' => 'users.store',
        'uses' => API_NS . 'UserController@store'
    ]);

    
});



