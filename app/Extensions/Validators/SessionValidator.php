<?php namespace App\Extensions\Validators;



use App\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\MessageBag;

class SessionValidator extends CustomValidator
{

    /**
     * Rules for basic validation.
     *
     * @var array
     */
    protected $rules = [
        'create' => [
            'email'    => 'required|email',
            'password' => 'required',
        ]
    ];

    /**
     * If valdiation succeed then this value will contain user.
     *
     * @var User
     */
    protected $user;

    public function user()
    {
        return $this->user;
    }

    /**
     * If validation fails then $errors variable should be filled with MessageBag instance.
     *
     * @param array $input
     *
     * @return bool
     */
    public function validate(array $input)
    {
        $validation = \Validator::make($input, $this->rules['create']);
        if ($validation->fails()) {
            $this->errors = $validation->messages();
            return false;
        }

        if ($this->isUserExist($input)) {
            return true;
        } else {
            $this->errors = new MessageBag([
                'login' => 'Incorrect email or password'
            ]);
            return false;
        }

    }

    /**
     * Validate user existence.
     * If user exists and credentials are valid then login user.
     *
     * @param array $input
     * @return bool
     */
    protected function isUserExist(array $input)
    {
        $user = $this->getUserByIdentities($input);

        if (!$user) {
            return false;
        }
        if (!$user->passwordCheck($input['password'])) {
            return false;
        }

        $this->user = $user;
        return true;
    }

    /**
     * Return user by identities.
     *
     * @param array $input
     * @return User
     */
    public function getUserByIdentities(array $input)
    {
        $user = new User();

        $firstWhere = true;
        foreach (Config::get('auth.identities') as $identity) {

            if ($firstWhere) {
                $user = $user->where($identity, '=', $input[$identity]);
            } else {
                $user = $user->orWhere($identity, '=', $input[$identity]);
            }

            $firstWhere = false;
        }

        $user = $user->get()->first();

        return $user;
    }

}