<?php namespace App\Extensions\Validators;


use Illuminate\Support\MessageBag;

abstract class CustomValidator
{
    /**
     * @var MessageBag
     */
    protected $errors;

    /**
     * If validation fails then $errors variable should be filled with MessageBag instance.
     *
     * @param array $input
     *
     * @return bool
     */
    abstract public function validate(array $input);

    /**
     * @return mixed null or MessageBag
     */
    public function errors()
    {
        return $this->errors;
    }
}