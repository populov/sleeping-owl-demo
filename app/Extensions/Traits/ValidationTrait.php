<?php namespace App\Extensions\Traits;

use Illuminate\Support\Facades\Validator;

trait ValidationTrait
{
    /**
     * @var \Illuminate\Support\MessageBag
     */
    protected static $errors;

    /**
     * @return \Illuminate\Support\MessageBag
     */
    public function errors()
    {
        return self::$errors;
    }

    /**
     * Return array of rules for model validation
     *
     * @return array
     */
    abstract public function getRules();

    /**
     * Validate model
     * If validation fails $errors property will be filled.
     *
     * @param array $input
     *
     * @return bool
     */
    public function isValid(array $input)
    {
        $validation = Validator::make($input, $this->getRules());

        if ($validation->passes()) {
            return true;
        }

        self::$errors = $validation->messages();
        return false;
    }
}