<?php 

namespace App\Extensions\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

trait SearchTrait
{
    /**
     * Return limit
     *
     * @param Request $request
     * @return int
     */
    private function getLimit(Request $request)
    {
        $maxLimit = Config::get('api.maxLimit');
        $limit = (int)$request->get('limit');
        $limit = $limit > $maxLimit ? $maxLimit : $limit;
        return $limit ?: 10;
    }


    /**
     * Return array of fields for select
     *
     * @param Request $request
     * @return array
     */
    private function getFields(Request $request)
    {
        $visibleFields = $this->getVisibleFields();
        $fields = [];
        if ($request->get('fields')) {
            $fields = explode(',', $request->get('fields'));
            foreach ($fields AS $k => $field) {
                $field = trim($field);
                if (!in_array($field, $visibleFields)) {
                    unset($fields[$k]);
                }
            }
        }
        if (empty($fields)) {
            $fields = ['*'];
        }
        return $fields;
    }


    /**
     * Return Builder object after apply filters
     *
     * @param object $model
     * @param Request $request
     * @return Builder
     */
    private function search($model, Request $request)
    {
        $searchFields = $this->getSearchFields();

        if (!$request->get('search') || !count($searchFields)) {
            return $model;
        }

        $keyword = $request->get('search');
        $isLike = false;
        if (mb_substr($keyword, 0, 1) === '*') {
            $keyword = '%' . mb_substr($keyword, 1, strlen($keyword));
            $isLike = true;
        }
        if (mb_substr($keyword, (strlen($keyword)-1), 1) === '*') {
            $keyword = substr($keyword, 0, (strlen($keyword)-1)) . '%';
            $isLike = true;
        }

        $model = $model->where(function($query) use ($searchFields, $keyword, $isLike){
            $isFirstWhere = true;
            foreach ($searchFields AS $field) {
                $operator = $isLike ? 'like' : '=';
                if ($isFirstWhere) {
                    $query = $query->where($field, $operator, $keyword);
                } else {
                    $query = $query->orWhere($field, $operator, $keyword);
                }
                $isFirstWhere = false;
            }
        });

        return $model;
    }


    /**
     * Return Builder object after apply order
     *
     * @param object $model
     * @param Request $request
     * @return Builder
     */
    private function sort($model, Request $request)
    {
        if (!$request->get('sort')) {
            return $model;
        }
        $visibleFields = $this->getVisibleFields();
        $sortFields = explode(',', $request->get('sort'));
        foreach ($sortFields AS $field) {
            $field = trim($field);
            $direction = 'asc';
            if (mb_substr($field, 0, 1) === '-') {
                $direction = 'desc';
                $field = mb_substr($field, 1, strlen($field));
            }
            if (in_array($field, $visibleFields)) {
                $model = $model->orderBy($field, $direction);
            }
        }
        return $model;
    }


    private function addUrlParams(LengthAwarePaginator $paginator, Request $request)
    {
        $additionalParams = [
            'limit',
            'fields',
            'search',
            'sort'
        ];
        foreach ($additionalParams AS $param) {
            if (!empty($request->get($param))) {
                $paginator->appends($param, $request->get($param));
            }
        }
        return $paginator;
    }


    /**
     * Return result of paginate function
     *
     * @param Request $request
     * @param object $model
     * @return object
     */
    public function getList(Request $request, $model = null)
    {
        if (empty($model)) {
            $model = $this->getModel();
        }
        $limit = $this->getLimit($request);
        $fields = $this->getFields($request);

        $model = $this->search($model, $request);
        $model = $this->sort($model, $request);
        $paginator = $model->paginate($limit, $fields);
        return $this->addUrlParams($paginator, $request);
    }
}