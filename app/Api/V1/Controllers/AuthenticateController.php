<?php

namespace App\Api\V1\Controllers;
use Illuminate\Http\Request;
use App\Model\Repositories\UserRepository;


/**
 * Authenticate resource representation.
 *
 * @Resource("Authenticate", uri="/api/v1")
 */
class AuthenticateController extends BaseController
{
    public $repo;


    /**
     * Construct method. Define repo of resource
     *
     * @param UserRepository $repo
     */
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }


    /**
     * Authenticate user
     *
     * Authenticate user by `email` and `password`.
     *
     * @Post("/login")
     * @Versions({"v1"})
     * @Request({"email": "email@example.com", "password": "123456"}, headers={"Content-Type": "application/json"})
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        return $this->response->array(['token' => $this->repo->JWTAuth($credentials)]);
    }


    /**
     * Logout user
     *
     * Invalidate access token
     *
     * @Delete("/logout")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {{AccessToken}}"})
     */
    public function logout()
    {
        \JWTAuth::invalidate(\JWTAuth::getToken());
        return $this->success();
    }


}