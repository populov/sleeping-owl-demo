<?php

namespace App\Api\V1\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;


class BaseController extends Controller
{
    use Helpers;

    public function success( $data = null, $message = null)
    {
        $content = [];
        
        if ($data !== null)
            $content['data'] = $data;

        if ($message !== null)
            $content['message'] = $message;

        return $this->response->array($content);
    }
}
