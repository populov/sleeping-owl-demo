<?php

namespace App\Api\V1\Controllers;
use App\Api\V1\Transformers\BaseTransformer;
use App\User;
use App\Model\Repositories\UserRepository;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * User resource representation.
 *
 * @Resource("Users", uri="/api/v1/users")
 */
class UserController extends BaseController
{
    public $repo;

    
    /**
     * Construct method. Define repo of resource
     *
     * @param UserRepository $repo
     */
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }


    /**
     * Register user
     *
     * Register a new user with a `email` and `password`.
     *
     * @Post("/")
     * @Versions({"v1"})
     * @Request({"email": "email@example.com", "password": "123456", "name": "Name of user"}, headers={"Content-Type": "application/json"})
     */
    public function store()
    {
        if ($user = $this->repo->store(null, Input::all())) {
            return $this->response->item($user, new BaseTransformer);
        } else {
            throw new StoreResourceFailedException('Could not create new user.', $this->repo->errors());
        }
    }


    /**
     * Update user
     *
     * @Put("/{id}")
     * @Versions({"v1"})
     * @Request({"email": "email@example.com", "password": "123456", "name": "Name of user"}, headers={"Authorization": "Bearer {{AccessToken}}", "Content-Type": "application/json"})
     */
    public function update($id)
    {
        if ($id != $this->user->id) {
            throw new AccessDeniedHttpException;
        }
        if ($user = $this->repo->store($id, Input::all())) {
            return $this->response->item($user, new BaseTransformer);
        } else {
            throw new UpdateResourceFailedException('Could not update user.', $this->repo->errors());
        }
    }

    /**
     * Delete user
     *
     * @Delete("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {{AccessToken}}", "Content-Type": "application/json"})
     */
    public function destroy($id)
    {
        if ($this->repo->destroy($id)) {
            return $this->success();
        } else {
            throw new DeleteResourceFailedException('Could not update user.', $this->repo->errors());
        }
    }


    /**
     * Show all users
     *
     * Get a JSON representation of all the registered users.
     *
     * @Get("/{?page,limit,fields,search,sort}")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {{AccessToken}}"})
     * @Parameters({
     *      @Parameter("page", type="integer", description="The page of results to view.", default=1),
     *      @Parameter("limit", type="integer", description="The amount of results per page.", default=10),
     *      @Parameter("fields", description="List of fields separated by comma", default="*"),
     *      @Parameter("search", description="Search keyword by searcheble fields", default="none"),
     *      @Parameter("sort", description="List of sort fields separated by comma", default="none")
     * })
     *
     */
    public function index(Request $request)
    {
        return $this->response->paginator($this->repo->getList($request), new BaseTransformer);
    }


    /**
     * Show user details
     *
     * @get("/{id}")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {{AccessToken}}", "Content-Type": "application/json"})
     */
    public function show($id)
    {
        return $this->response->item($this->repo->getProfile($id, $this->user), new BaseTransformer);
    }

    /**
     * Show authenticated user details
     *
     * @get("/profile")
     * @Versions({"v1"})
     * @Request(headers={"Authorization": "Bearer {{AccessToken}}", "Content-Type": "application/json"})
     */
    public function profile()
    {
        return $this->response->item($this->repo->getProfile($this->user->id, $this->user), new BaseTransformer);
    }
}