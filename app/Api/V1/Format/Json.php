<?php

namespace App\Api\V1\Format;

use Illuminate\Support\Str;
use Illuminate\Contracts\Support\Arrayable;

class Json extends \Dingo\Api\Http\Response\Format\Json
{
    /**
     * Encode the content to its JSON representation.
     *
     * @param string $content
     *
     * @return string
     */
    protected function encode($content)
    {
        if (!isset($content['status'])) {
            $content = ['status' => 'success'] + $content;
        }
        return json_encode($content);
    }
}
