<?php

namespace App\Providers;

use AdminTemplate;
use App\Model\Inquire;
use App\Model\MarketPlace;
use App\Model\Placement;
use App\Model\Template;
use App\User;
use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{
    /**
     * Sections mapping
     *
     * {@link http://sleepingowl.laravel.su/docs/4.0/model_configuration_section}
     *
     * @var array
     */
    protected $sections = [
        Inquire::class => \App\Admin\Inquire::class,
        MarketPlace::class => \App\Admin\MarketPlace::class,
        Template::class => \App\Admin\Template::class,
        User::class => \App\Admin\User::class,
        Placement::class => \App\Admin\Placement::class
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
        parent::boot($admin);

        $this->injectProfile();
    }

    /**
     * Inject user name and logout button into top navigation panel
     * TODO I don't see this changes. It works? (c) V.Kravtsov
     */
    private function injectProfile()
    {
        view()->composer(AdminTemplate::getViewPath('_partials.header'), function($view) {
            $view->getFactory()->inject(
                'navbar.right', view('admin.navbar', [
                    'user' => auth()->user()
                ])
            );
        });
    }
}