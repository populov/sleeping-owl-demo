/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases V8.1.2                     */
/* Target DBMS:           PostgreSQL 9                                    */
/* Project file:          AH_ERD.dez                                      */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Database drop script                            */
/* Created on:            2016-08-16 14:20                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Drop foreign key constraints                                           */
/* ---------------------------------------------------------------------- */

ALTER TABLE market_place DROP CONSTRAINT FK_market_place_templates;

ALTER TABLE inquires DROP CONSTRAINT FK_inquires_templates;

ALTER TABLE inquires DROP CONSTRAINT FK_inquires_market_place;

ALTER TABLE placements DROP CONSTRAINT FK_placements_inquires;

ALTER TABLE placements DROP CONSTRAINT FK_placements_market_place;

/* ---------------------------------------------------------------------- */
/* Drop table "placements"                                                */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE placements DROP CONSTRAINT PK_placements;

DROP TABLE placements;

/* ---------------------------------------------------------------------- */
/* Drop table "inquires"                                                  */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE inquires DROP CONSTRAINT PK_inquires;

DROP TABLE inquires;

/* ---------------------------------------------------------------------- */
/* Drop table "market_place"                                              */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE market_place DROP CONSTRAINT PK_market_place;

DROP TABLE market_place;

/* ---------------------------------------------------------------------- */
/* Drop table "templates"                                                 */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE templates DROP CONSTRAINT PK_templates;

DROP TABLE templates;

/* ---------------------------------------------------------------------- */
/* Drop table "users"                                                     */
/* ---------------------------------------------------------------------- */

/* Drop constraints */

ALTER TABLE users DROP CONSTRAINT PK_users;

DROP TABLE users;
