/* ---------------------------------------------------------------------- */
/* Script generated with: DeZign for Databases V8.1.2                     */
/* Target DBMS:           PostgreSQL 9                                    */
/* Project file:          AH_ERD.dez                                      */
/* Project name:                                                          */
/* Author:                                                                */
/* Script type:           Database creation script                        */
/* Created on:            2016-08-16 14:20                                */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- */
/* Add tables                                                             */
/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */
/* Add table "users"                                                      */
/* ---------------------------------------------------------------------- */

CREATE TABLE users (
    id SERIAL  NOT NULL,
    name CHARACTER VARYING(100)  NOT NULL,
    email CHARACTER VARYING(100)  NOT NULL,
    password CHARACTER VARYING(255)  NOT NULL,
    remember_token CHARACTER(64),
    created_at TIMESTAMP  NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    CONSTRAINT PK_users PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Add table "templates"                                                  */
/* ---------------------------------------------------------------------- */

CREATE TABLE templates (
    id SERIAL  NOT NULL,
    name CHARACTER VARYING(40)  NOT NULL,
    background CHARACTER VARYING(40)  NOT NULL,
    placeholder BOX,
    created_at TIMESTAMP  NOT NULL,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    CONSTRAINT PK_templates PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Add table "market_place"                                               */
/* ---------------------------------------------------------------------- */

CREATE TABLE market_place (
    id BIGSERIAL  NOT NULL,
    template_id INTEGER  NOT NULL,
    state CHARACTER VARYING(40),
    city CHARACTER VARYING(40),
    direction BOOLEAN,
    country CHARACTER VARYING(40),
    route CHARACTER(40),
    mile SMALLINT,
    location POINT,
    sign_id BIGINT,
    zip CHARACTER(6),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    CONSTRAINT PK_market_place PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Add table "inquires"                                                   */
/* ---------------------------------------------------------------------- */

CREATE TABLE inquires (
    id BIGSERIAL  NOT NULL,
    email CHARACTER VARYING(100)  NOT NULL,
    name CHARACTER VARYING(100)  NOT NULL,
    company_name CHARACTER VARYING(40),
    company_address CHARACTER VARYING(40),
    phone CHARACTER VARYING(15),
    template_id INTEGER,
    custom_logo CHARACTER VARYING(40)  NOT NULL,
    market_place_id BIGINT,
    created_at TIMESTAMP  NOT NULL,
    updated_at TIMESTAMP,
    CONSTRAINT PK_inquires PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Add table "placements"                                                 */
/* ---------------------------------------------------------------------- */

CREATE TABLE placements (
    id BIGSERIAL  NOT NULL,
    market_place_id INTEGER  NOT NULL,
    inquire_id BIGINT  NOT NULL,
    payed_since DATE,
    payed_until DATE  NOT NULL,
    deleted_at TIMESTAMP,
    CONSTRAINT PK_placements PRIMARY KEY (id)
);

/* ---------------------------------------------------------------------- */
/* Add foreign key constraints                                            */
/* ---------------------------------------------------------------------- */

ALTER TABLE market_place ADD CONSTRAINT FK_market_place_templates 
    FOREIGN KEY (template_id) REFERENCES templates (id);

ALTER TABLE inquires ADD CONSTRAINT FK_inquires_templates 
    FOREIGN KEY (template_id) REFERENCES templates (id);

ALTER TABLE inquires ADD CONSTRAINT FK_inquires_market_place 
    FOREIGN KEY (market_place_id) REFERENCES market_place (id);

ALTER TABLE placements ADD CONSTRAINT FK_placements_inquires 
    FOREIGN KEY (id) REFERENCES inquires (id);

ALTER TABLE placements ADD CONSTRAINT FK_placements_market_place 
    FOREIGN KEY (id) REFERENCES market_place (id);
