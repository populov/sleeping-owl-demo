<?php
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    private function process(array &$queries)
    {
        foreach ($queries as $query) {
            DB::statement($query);
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $queries = ['
            CREATE TABLE users (
                id SERIAL  NOT NULL,
                name CHARACTER VARYING(100)  NOT NULL,
                email CHARACTER VARYING(100)  NOT NULL,
                password CHARACTER VARYING(255)  NOT NULL,
                remember_token CHARACTER(64),
                created_at TIMESTAMP  NOT NULL,
                updated_at TIMESTAMP,
                deleted_at TIMESTAMP,
                CONSTRAINT PK_users PRIMARY KEY (id)
            );',
            '
            CREATE TABLE templates (
                id SERIAL  NOT NULL,
                name CHARACTER VARYING(40)  NOT NULL,
                background CHARACTER VARYING(255)  NOT NULL,
                placeholder BOX,
                created_at TIMESTAMP  NOT NULL,
                updated_at TIMESTAMP,
                deleted_at TIMESTAMP,
                CONSTRAINT PK_templates PRIMARY KEY (id)
            );',
            '
            CREATE TABLE market_places (
                id BIGSERIAL  NOT NULL,
                template_id INTEGER  NOT NULL,
                state CHARACTER VARYING(40),
                city CHARACTER VARYING(40),
                direction BOOLEAN,
                country CHARACTER VARYING(40),
                route CHARACTER(40),
                mile SMALLINT,
                location POINT,
                sign_id BIGINT,
                zip CHARACTER(6),
                created_at TIMESTAMP,
                updated_at TIMESTAMP,
                deleted_at TIMESTAMP,
                CONSTRAINT PK_market_places PRIMARY KEY (id)
            );',
                        '
            CREATE TABLE inquires (
                id BIGSERIAL  NOT NULL,
                email CHARACTER VARYING(100)  NOT NULL,
                name CHARACTER VARYING(100)  NOT NULL,
                company_name CHARACTER VARYING(40),
                company_address CHARACTER VARYING(40),
                phone CHARACTER VARYING(15),
                template_id INTEGER,
                custom_logo CHARACTER VARYING(255)  NOT NULL,
                market_place_id BIGINT,
                created_at TIMESTAMP  NOT NULL,
                updated_at TIMESTAMP,
                CONSTRAINT PK_inquires PRIMARY KEY (id)
            );',
                        '
            CREATE TABLE placements (
                id BIGSERIAL  NOT NULL,
                market_place_id INTEGER  NOT NULL,
                inquire_id BIGINT  NOT NULL,
                payed_since DATE,
                payed_until DATE  NOT NULL,
                deleted_at TIMESTAMP,
                CONSTRAINT PK_placements PRIMARY KEY (id)
            );',
                        '
            ALTER TABLE market_places ADD CONSTRAINT FK_market_places_templates
                FOREIGN KEY (template_id) REFERENCES templates (id);',
                        '
            ALTER TABLE inquires ADD CONSTRAINT FK_inquires_templates
                FOREIGN KEY (template_id) REFERENCES templates (id);',
                        '
            ALTER TABLE inquires ADD CONSTRAINT FK_inquires_market_places
                FOREIGN KEY (market_place_id) REFERENCES market_places (id);',
                        '
            ALTER TABLE placements ADD CONSTRAINT FK_placements_inquires
                FOREIGN KEY (id) REFERENCES inquires (id);',
                        '
            ALTER TABLE placements ADD CONSTRAINT FK_placements_market_places
                FOREIGN KEY (id) REFERENCES market_places (id);',
        ];

        $this->process($queries);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $queries = ['ALTER TABLE market_places DROP CONSTRAINT FK_market_places_templates;',
            'ALTER TABLE inquires DROP CONSTRAINT FK_inquires_templates;',
            'ALTER TABLE inquires DROP CONSTRAINT FK_inquires_market_places;',
            'ALTER TABLE placements DROP CONSTRAINT FK_placements_inquires;',
            'ALTER TABLE placements DROP CONSTRAINT FK_placements_market_places;',
            'ALTER TABLE placements DROP CONSTRAINT PK_placements;',
            'DROP TABLE placements;',
            'ALTER TABLE inquires DROP CONSTRAINT PK_inquires;',
            'DROP TABLE inquires;',
            'ALTER TABLE market_places DROP CONSTRAINT PK_market_places;',
            'DROP TABLE market_places;',
            'ALTER TABLE templates DROP CONSTRAINT PK_templates;',
            'DROP TABLE templates;',
            'ALTER TABLE users DROP CONSTRAINT PK_users;',
            'DROP TABLE users;',
        ];

        $this->process($queries);
    }
}
